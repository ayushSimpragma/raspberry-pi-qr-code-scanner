# python version: 3+
# code_scanner.py

# importing necessary packages
from imutils.video import VideoStream
from pyzbar import pyzbar
import argparse
import datetime
import imutils
import time
import cv2

	
try:
	# constructing the argument parser and parsing the arguments
	argument_parser = argparse.ArgumentParser()
	argument_parser.add_argument("-o", "--output", type = str, default = "found_codes.txt", help = "path to output CSV file containing codes")
	arguments = vars(argument_parser.parse_args())
except Exception as ex:
	print("<< SOMETHING WRONG WITH ARGUMENT PARSER >>")
	print(ex)

try:	
	# initializing the video stream from camera and allowing 3 seconds for the camera sensor to boot up
	print("[INFO] Started video stream >> ")
	video_stream = VideoStream(usePiCamera = True).start()
	# video_stream = VideoStream(src = 0).start()         # use this instead, if not using raspberry pi       
	time.sleep(3.0)
except Exception as ex:
	print("<< SOMETHING WRONG WITH CAMERA INITIALIZATION >>")
	print(ex)

try:	
	# opening the text file for writing and initializing the set of codes
	found_codes_text_file = open(arguments["output"], "w")
	found = set() # storing the found codes as set of strings in order to avoid redundancy of strings 
except Exception as ex:
	print("<< SOMETHING WRONG WITH THE OUTPUT FILE >>")
	print(ex)

print("[INFO] Frame processing started >>")
try:
	# iterating over the frames that are present in the video_stream
	while True:
		# grabing the frame from the threaded video_stream and resizing it 
		# have a maximum width of 500 pixels to reduce load on the processor
		frame = video_stream.read()
		frame = imutils.resize(frame, width = 500) # one can also adjust it according to requirement

		# finding type of codes in the frame and decode each of the codes
		codes = pyzbar.decode(frame)

		# iterating over the detected codes
		for code in codes:
			# extracting the bounding box location of the code 
			# and projecting the box around the codes that are present in the image
			(x, y, w, h) = code.rect    # code.rect -->> returns a tuple with coordinates and width and height                 
			cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2) # drawing the rectangle 
        
			# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< NOTE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#
			#                                                                                #
			#          the code data is a bytes object and thus we cannot directly           # 
			#            print it on the screen, so in order to draw it on screen            #
			#              this bytes object is needed to be converted to string             #
			#                                                                                #
			##################################################################################

			codeData = code.data.decode("utf-8") # formatting bytes object to string
			codeType = code.type # this could be a barcode or a qr code
		
			##################################################################################
			#                                                                                #
			#       add code snippet to add more functionality like triggering an            # 
			#       event or sending a message to some socket in this part of the script     #
			#                                                                                #
			##################################################################################

			# drawing the code data and code type on the frame
			text_to_be_displayed_on = "{} ({})".format(codeData, codeType)
			cv2.putText(frame, text_to_be_displayed_on, (x, y - 10),
				cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)

			# if the code text is currently not in our CSV file, write
			# the timestamp + code to disk and update the set
			if codeData not in found:
				found_codes_text_file.write("{},{}\n".format(datetime.datetime.now(), codeData))
				found_codes_text_file.flush()
				found.add(codeData)

		# showing the output frame 
		# (if don't want to show the frame comment the below line with cv2.imshow() function) 
		cv2.imshow("Code Scanner Output", frame)
		key = cv2.waitKey(1) & 0xFF
 
		# if one presses `q` key, break from the loop
		if key == ord("q"):
			break

except Exception as ex:
	print("<< SOMETHING WRONG WITH FRAME PROCESSING >>")
	print(ex)

try:
	# closing the output text file and doing cleanup
	print("[INFO] Cleaning up >>")
	found_codes_text_file.close()
	cv2.destroyAllWindows()
	video_stream.stop()
except Exception as ex:
	print("<< SOMETHING WENT WRONG WHILE CLOSING RESOURCES  >>")

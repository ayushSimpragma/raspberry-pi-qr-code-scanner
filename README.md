# raspberry-pi-qr-code-scanner

There are two files present in this repo:
1. code_scanner.py: python script that implements the functionality
2. found_codes.txt: this file is empty, it is intended to store the strings encoded in 
the QR codes with date and time but one can use any other file as ouput by providing 
the file path as argument after "-o" option

